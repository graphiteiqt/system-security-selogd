/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define LOG_TAG "selogd"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/capability.h>
#include <sys/klog.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <fcntl.h>
#include <dirent.h>
#include <private/android_filesystem_config.h>
#include <linux/capability.h>
#include <linux/prctl.h>

#include "cutils/klog.h"
#include "cutils/log.h"
#include "cutils/properties.h"
#include "selogcat.h"

// The SELOGD names need to be kept in sync with:
// packages/apps/SEReportService/src/com/android/sereports/SEReportService.java & SEReportApplication.java
#define AUDITD_LOG_DIR      "/data/misc/audit"
#define AUDITD_LOG_FILE     AUDITD_LOG_DIR "/audit.log"
#define AUDITD_OLD_LOG_FILE AUDITD_LOG_DIR "/audit.old"
#define SELOGD_ERRS         AUDITD_LOG_DIR "/selogd_errs.txt"
#define SELOGD_ALL          AUDITD_LOG_DIR "/selogd_all.txt";

#define KLOG_BUF_SHIFT	17	/* CONFIG_LOG_BUF_SHIFT from our kernel */
#define KLOG_BUF_LEN	(1 << KLOG_BUF_SHIFT)

static int g_outFD = -1;


#define RAISE(ary, c) ary[CAP_TO_INDEX(c)].permitted |= CAP_TO_MASK(c);

static void drop_privileges_or_die(void)
{

    struct __user_cap_header_struct capheader;
    struct __user_cap_data_struct capdata[2];

    if (prctl(PR_SET_KEEPCAPS, 1) < 0) {
        SLOGE("Failed on prctl KEEPCAPS: %s", strerror(errno));
        exit(1);
    }

    if (setgid(AID_AUDIT) < 0) {
        SLOGE("Failed on setgid: %s", strerror(errno));
        exit(1);
    }

    if (setuid(AID_AUDIT) < 0) {
        SLOGE("Failed on setuid: %s", strerror(errno));
        exit(1);
    }

    memset(&capheader, 0, sizeof(capheader));
    memset(&capdata, 0, sizeof(capdata));
    capheader.version = _LINUX_CAPABILITY_VERSION_3;
    capheader.pid = 0;

    RAISE(capdata, CAP_AUDIT_CONTROL);
    RAISE(capdata, CAP_SYSLOG);

    capdata[0].effective = capdata[0].permitted;
    capdata[1].effective = capdata[1].permitted;
    capdata[0].inheritable = 0;
    capdata[1].inheritable = 0;

    if (capset(&capheader, &capdata[0]) < 0) {
        SLOGE("Failed on capset: %s", strerror(errno));
        exit(1);
    }
}

static int dmesg_main(char* buffer);

int dgrep(const char* grep) 
{
    char buffer[KLOG_BUF_LEN + 1];
    char *c = buffer;
    // char *linep;
    int n,k;

    n = dmesg_main(buffer);
    //    write(STDOUT_FILENO,buffer,n);
    c = strtok(buffer,"\n");
    while(c != NULL) {
      k = strlen(c);
      // write(STDOUT_FILENO,c,k);

      if(strstr(c,grep)) {
	// write(STDOUT_FILENO,c,k);
	// write(STDOUT_FILENO,"\n",1);
	write(g_outFD,c,k);
	write(g_outFD,"\n",1);
      }

      c = strtok(NULL,"\n");
    }

    return 0;
}

static int dmesg_main(char* buffer)
{
    int n;

    n = klogctl(KLOG_READ_ALL, buffer, KLOG_BUF_LEN);
    if (n < 0) {
        perror("klogctl");
        return EXIT_FAILURE;
    }
    buffer[n] = '\0';

    return n;
}

static int openLogFile (const char *pathname) {
  return open(pathname, O_WRONLY | O_APPEND | O_CREAT, S_IRUSR | S_IWUSR);
}


#define LINE_LEN 255

static void parse_audit_file(const char* audit_file, const char* grep) {
  char line[LINE_LEN];
  FILE* fd;

  fd = fopen(audit_file, "r");
  if(fd == NULL) { return; }

  while(fgets(line, sizeof(line), fd)) {
    char* c;
    int k;
    c = strtok(line,"\n");
    while(c != NULL) {
      k = strlen(c);
      if(strstr(c,grep)) {
	write(g_outFD,c,k);
	write(g_outFD,"\n",1);
      }

      c = strtok(NULL,"\n");
    }
  }

  fclose(fd);
}


static void check_for_SE_errors(const char* filename) {
  const char *allSEErrors = "SELinuxMMAC:E SELinux:E";
  android::g_outputFileName = filename;
  g_outFD = openLogFile(android::g_outputFileName);

  selogcat_main(allSEErrors);                   // grep logcat for all SELinux messages

  parse_audit_file(AUDITD_LOG_FILE,"denied");   // grep audit.log for all denied messages

  close(g_outFD);
  chmod(android::g_outputFileName, 0644);
}

static void write_selogs(const char* filename) {
  time_t rawtime;
  struct tm* timeinfo;
  char* asc_time;
  const char *allSEFilters = "SELinuxMMAC:V SELinux:V";
  android::g_outputFileName = filename;
  g_outFD = openLogFile(android::g_outputFileName);

  time(&rawtime);
  timeinfo = localtime(&rawtime);
  asc_time = asctime(timeinfo);
  write(g_outFD,asc_time,strlen(asc_time));
  write(g_outFD,"\n------- dmesg SELinux ----------------------------------------------\n",70);
  dgrep("SELinux");                            // grep dmesg for 'SELinux'

  write(g_outFD,"\n------- logcat SELinuxMMAC SELinux ---------------------------------\n",70);
  selogcat_main(allSEFilters);                // grep logcat for all SELinux messages

  write(g_outFD,"\n------- auditd.log -------------------------------------------------\n",70);
  parse_audit_file(AUDITD_LOG_FILE,"");       // include all of auditd.log
  
  write(g_outFD,"\n------- auditd.old -------------------------------------------------\n",70);
  parse_audit_file(AUDITD_OLD_LOG_FILE,"");   // include all of auditd.old
  
  close(g_outFD);
  chmod(android::g_outputFileName, 0644);
}

int main() {
  const char* err_log = SELOGD_ERRS;
  const char* all_log = SELOGD_ALL; 

  int hourCount = 12;

  SLOGI("selogd starting");

  drop_privileges_or_die();

  // setLogFormat("time");

  while(1) {
    SLOGI("selogd logging SE activity");

    // reset full log each time
    unlink(all_log);

    if(hourCount <= 0) {
      // append error log until 1 hour, then reset the file
      unlink(err_log);
      hourCount = 12;
    }

    check_for_SE_errors(err_log);
    write_selogs(all_log);

    sleep(5 * 360);  // this is seconds - every 5 mins
    --hourCount;
  }

  SLOGI("selogd exiting");
  exit(0);
}
